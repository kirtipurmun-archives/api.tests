using System;
using Xunit;
using KirtipurMun.Mpt.Api.Models;

namespace  KirtipurMun.Mpt.Api.tests
{
    public class Api_IsProjectShould
    {

        const string projectTestName = "Test project";
        const string projectTestStatus = "testing";
        private readonly Project _project;

        public Api_IsProjectShould()
        {
          _project = new Project() {
            Name = projectTestName,
            Status = projectTestStatus
          };
        }

        [Fact]
        public void ProjectHasName()
        {
          Assert.True(_project.Name == projectTestName, "project bears assigned name");
        }
        [Fact]
        public void ProjectHasStatus()
        {
          Assert.True(_project.Status == projectTestStatus, "project assigned with correct status");
        }
    }
}
